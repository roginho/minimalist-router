#!/bin/bash

# Verifica se o diretório "coverage" existe; se não, cria-o
if [ ! -d ".coverage-report" ]; then
    mkdir .coverage-report
fi
vendor/bin/phpunit --coverage-html .coverage-report
