<?php

namespace Minimalist\Router;

use ReflectionClass;
use ReflectionException;
use Psr\Container\ContainerInterface;

class ControllerFactory
{
    private ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function create(string $class)
    {
        try {
            $reflector = new ReflectionClass($class);

            if (!$reflector->isInstantiable()) {
                throw new \RuntimeException("Class $class is not instantiable");
            }

            $constructor = $reflector->getConstructor();

            if ($constructor === null) {
                return new $class;
            }

            $parameters = $constructor->getParameters();
            $dependencies = [];

            foreach ($parameters as $parameter) {
                $type = $parameter->getType();
                if ($type && !$type->isBuiltin()) {
                    $className = $type->getName();

                    // Ensure the class is registered in the container
                    if (!$this->container->has($className)) {
                        throw new \RuntimeException("Cannot resolve class: $className");
                    }

                    $dependencies[] = $this->container->get($className);
                } else {
                    throw new \RuntimeException("Cannot resolve built-in type: {$parameter->getName()}");
                }
            }

            return $reflector->newInstanceArgs($dependencies);

        } catch (ReflectionException $e) {
            throw new \RuntimeException("Error resolving dependencies for $class: " . $e->getMessage());
        }
    }
}
