<?php

namespace Minimalist\Router\Exceptions;

class RouteMethodNotFoundException extends \Exception
{

    public function __construct()
    {
        parent::__construct('HTTP method not allowed', 405);
    }
}