<?php
namespace Minimalist\Router;

use Minimalist\Router\Exceptions\RouteMethodNotFoundException;
use Minimalist\Router\Exceptions\RouteNotFoundException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class Router implements RequestHandlerInterface
{
    private array $routes = [];
    private ControllerFactory $controllerFactory;
    
    private array $middleware = [];

    public function __construct(ControllerFactory $controllerFactory)
    {
        $this->controllerFactory = $controllerFactory;
    }

    public function addMiddleware(MiddlewareInterface $middleware): void
    {
        $this->middleware[] = $middleware;
    }

    private function addRoute(string $method, string $path, callable|string|array $handler): void
    {
        $path = preg_replace('/{([a-zA-Z0-9_-]+)}/', '(?<$1>[^/]+)', $path);
        $regexPath = '#^' . $path . '$#i';
        $this->routes[$method][$regexPath] = $handler;
    }

    public function get(string $path, callable|string|array $handler): void
    {
        $this->addRoute('GET', $path, $handler);
    }

    public function post(string $path, callable|string|array $handler): void
    {
        $this->addRoute('POST', $path, $handler);
    }

    public function put(string $path, callable|string|array $handler): void
    {
        $this->addRoute('PUT', $path, $handler);
    }

    public function patch(string $path, callable|string|array $handler): void
    {
        $this->addRoute('PATCH', $path, $handler);
    }

    public function delete(string $path, callable|string|array $handler): void
    {
        $this->addRoute('DELETE', $path, $handler);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        // Função de dispatch encapsulada no RequestHandlerWrapper
        $handler = new RequestHandlerWrapper(function (ServerRequestInterface $request) {
            return $this->dispatch($request);
        });

        // Envolvendo o handler com middlewares
        foreach (array_reverse($this->middleware) as $middleware) {
            $handler = new RequestHandlerWrapper(function (ServerRequestInterface $request) use ($middleware, $handler) {
                return $middleware->process($request, $handler);
            });
        }

        return $handler->handle($request);
    }
    private function dispatch(ServerRequestInterface $request): ResponseInterface
    {
        $path = $request->getUri()->getPath();
        $method = $request->getMethod();

        if (!isset($this->routes[$method])) {
            throw new RouteMethodNotFoundException();
        }

        foreach ($this->routes[$method] as $route => $handler) {
            if (!preg_match($route, $path, $matches)) {
                continue;
            }

            array_shift($matches);
            $params = [$request, ...array_values($matches)];

            return $this->handleRoute($handler, $params);
        }

        throw new RouteNotFoundException();
    }

    private function handleRoute(callable|string|array $handler, array $params): ResponseInterface
    {
        if (is_array($handler)) {
            if (count($handler) === 2 && is_string($handler[0]) && is_string($handler[1])) {
                [$controllerClass, $action] = $handler;

                if (!class_exists($controllerClass)) {
                    throw new \RuntimeException('Controller class does not exist');
                }

                $controller = $this->controllerFactory->create($controllerClass);

                if (!method_exists($controller, $action)) {
                    throw new \RuntimeException('Controller action does not exist');
                }

                return $controller->$action(...$params);
            }
        }

        if (is_string($handler)) {
            if (!class_exists($handler)) {
                throw new \RuntimeException('Controller class does not exist');
            }

            $controller = $this->controllerFactory->create($handler);

            if (!is_callable($controller)) {
                throw new \RuntimeException('Controller is not callable');
            }

            return $controller(...$params);
        }

        if (is_callable($handler)) {
            return $handler(...$params);
        }

        throw new \RuntimeException('Invalid route handler');
    }
}
