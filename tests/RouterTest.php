<?php

use Minimalist\Router\ControllerFactory;
use Minimalist\Router\Exceptions\RouteMethodNotFoundException;
use Minimalist\Router\Exceptions\RouteNotFoundException;
use Minimalist\Router\Router;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;

class RouterTest extends TestCase
{
    private Router $router;

    protected function setUp(): void
    {
        parent::setUp();
        $containerInterfaceMock = $this->createMock(ContainerInterface::class);
        $controllerFactory = new ControllerFactory($containerInterfaceMock);
        $this->router = new Router($controllerFactory);
    }

    /**
     * @dataProvider routes
     */
    public function testAddRouteAndGetOrPost($data)
    {
        $method = $data['method'];
        $path = $data['path'];
        $expectedStatusCode = $data['expectedStatusCode'];
        $expectedBody = $data['expectedBody'];
        
        // Mock da resposta
        $responseMock = $this->createMock(ResponseInterface::class);
        $responseMock->method('getStatusCode')->willReturn($expectedStatusCode);
        $responseMock->method('getBody')->willReturn($expectedBody);

        // Handler que retorna o mock
        $handler = function (ServerRequestInterface $request) use ($responseMock) {
            return $responseMock;
        };

        // Adiciona a rota com o handler mockado
        call_user_func_array([$this->router, $method], [$path, $handler]);

        // Mock da requisição
        $request = $this->createMock(ServerRequestInterface::class);
        $request->method('getMethod')->willReturn($method);
        $request->method('getUri')->willReturn($this->createMockUri($path));

        // Chama o método handle da rota
        $result = $this->router->handle($request);

        // Asserts para verificar se a resposta é a esperada
        $this->assertInstanceOf(ResponseInterface::class, $result);
        $this->assertEquals($expectedStatusCode, $result->getStatusCode());
        $this->assertEquals($expectedBody, (string) $result->getBody());
    }

 

    public function testRouteMethodNotFoundException()
    {
        $this->expectException(RouteMethodNotFoundException::class);
        $request = $this->createMock(ServerRequestInterface::class);
        $request->method('getMethod')->willReturn('OPTIONS');
        $request->method('getUri')->willReturn($this->createMockUri('/notregistered'));
        $this->router->handle($request);
    }

    public function testHandleRouteNotFoundException()
    {
        $this->expectException(RouteNotFoundException::class);
        $request = $this->createMock(ServerRequestInterface::class);
        $responseMock = $this->createMock(ResponseInterface::class);
        $handler = function (ServerRequestInterface $request) use ($responseMock) {
            return $responseMock;
        };
        $this->router->get('/known-route', $handler);
        $request = $this->createMock(ServerRequestInterface::class);
        $request->method('getMethod')->willReturn('GET');
        $request->method('getUri')->willReturn($this->createMockUri('/unknown-route'));
        $this->router->handle($request);
    }

    /**
     * @dataProvider  invalidRouter
     */
    public function testHandleRouteWithNonExistingController($invalidRouter)
    {
        // Configura as expectativas para a exceção
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Controller class does not exist');

        // Cria mocks para a requisição e a resposta
        $request = $this->createMock(ServerRequestInterface::class);
        $uri = $this->createMock(\Psr\Http\Message\UriInterface::class);

        // Configura o mock da URI para retornar o caminho desejado
        $uri->method('getPath')->willReturn('/route');
        $request->method('getUri')->willReturn($uri);
        $request->method('getMethod')->willReturn('GET');

        // Adiciona a rota com um controlador não existente
        $this->router->get('/route', $invalidRouter);

        // Chama o método handle, que deve lançar a exceção
        $this->router->handle($request);
    }

    public function testHandleRouteWithNonExistingAction()
    {

        // Configura as expectativas para a exceção
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Controller action does not exist');

        // Cria mocks para a requisição e a URI
        $request = $this->createMock(ServerRequestInterface::class);
        $uri = $this->createMock(\Psr\Http\Message\UriInterface::class);

        // Configura o mock da URI para retornar o caminho desejado
        $uri->method('getPath')->willReturn('/test');
        $request->method('getUri')->willReturn($uri);
        $request->method('getMethod')->willReturn('GET');

        // Adiciona a rota com um controlador válido, mas método de ação inexistente
        $this->router->get('/test', [TestControllerNoAction::class, 'nonExistingMethod']);

        // Chama o método handle, que deve lançar a exceção
        $this->router->handle($request);
    }


    /**
     * @dataProvider  validRouter
     */
    public function testHandleRouteWithExistingAction($validRouter)
    {
        // Cria mocks para a requisição e a URI
        $request = $this->createMock(ServerRequestInterface::class);
        $uri = $this->createMock(\Psr\Http\Message\UriInterface::class);

        // Configura o mock da URI para retornar o caminho desejado
        $uri->method('getPath')->willReturn('/test');
        $request->method('getUri')->willReturn($uri);
        $request->method('getMethod')->willReturn('GET');

        // Adiciona a rota com um controlador e método de ação válidos
        $this->router->get('/test', $validRouter);

        // Chama o método handle, que deve retornar uma resposta com sucesso
        $response = $this->router->handle($request);

        // Verifica se a resposta é a esperada
        $this->assertInstanceOf(ResponseInterface::class, $response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('Success', (string)$response->getBody());
    }

  
    public function testHandleRouteWithInvalidHandler()
    {
        // Cria mocks para a requisição e a URI
        $request = $this->createMock(ServerRequestInterface::class);
        $uri = $this->createMock(\Psr\Http\Message\UriInterface::class);

        // Configura o mock da URI para retornar o caminho desejado
        $uri->method('getPath')->willReturn('/invalid');
        $request->method('getUri')->willReturn($uri);
        $request->method('getMethod')->willReturn('GET');

        // Adiciona a rota com um handler inválido
        $this->router->get('/invalid', [123, 'invalidHandler']);

        // Espera que a exceção RuntimeException seja lançada
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Invalid route handler');

        // Chama o método handle, que deve lançar uma exceção
        $this->router->handle($request);
    }

    public function testControllerNotCallableWithoutInvoque()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Controller is not callable');

        $request = $this->createMock(ServerRequestInterface::class);
        $uri = $this->createMock(\Psr\Http\Message\UriInterface::class);

        $uri->method('getPath')->willReturn('/test');
        $request->method('getUri')->willReturn($uri);
        $request->method('getMethod')->willReturn('GET');

        $this->router->get('/test', TestControllerNoAction::class);
        $this->router->handle($request);

    }

    // Novos testes para middleware
    public function testMiddlewareProcessesRequest(): void
    {
        $this->router->addMiddleware(new TestMiddleware());

        $this->router->get('/test', function (ServerRequestInterface $request): ResponseInterface {
            return new TestResponse(200, [], 'OK');
        });

        $request = $this->createMock(ServerRequestInterface::class);
        $request->method('getMethod')->willReturn('GET');
        $request->method('getUri')->willReturn($this->createMockUri('/test'));

        $response = $this->router->handle($request);

        $this->assertInstanceOf(ResponseInterface::class, $response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('OK', (string) $response->getBody());
    }

    public function testMiddlewareThrowsException(): void
    {
        $this->router->addMiddleware(new TestMiddleware(true));

        $this->router->get('/test', function (ServerRequestInterface $request): ResponseInterface {
            return new TestResponse(200, [], 'OK');
        });

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Middleware exception');

        $request = $this->createMock(ServerRequestInterface::class);
        $request->method('getMethod')->willReturn('GET');
        $request->method('getUri')->willReturn($this->createMockUri('/test'));

        $this->router->handle($request);
    }
    
    private function createMockUri(string $path): UriInterface
    {
        $uriMock = $this->createMock(UriInterface::class);
        $uriMock->method('getPath')->willReturn($path);
        return $uriMock;
    }

    public function routes()
    {
        return [
           'GET' => [['method'=>'GET', 'path'=>'/hello', 'expectedStatusCode' => 200, 'expectedBody' => 'Hello, World!']],
           'POST' => [['method'=>'POST', 'path'=>'/create', 'expectedStatusCode' => 201, 'expectedBody' =>'Created']],
           'PUT' => [['method'=>'PUT', 'path'=>'/modify/1', 'expectedStatusCode' => 200, 'expectedBody' =>'Updated']],
           'PATCH' => [['method'=>'PATCH', 'path'=>'/modify/1', 'expectedStatusCode' => 200, 'expectedBody' =>'Updated']],
           'DELETE' => [['method'=>'DELETE', 'path'=>'/delete/1', 'expectedStatusCode' => 204, 'expectedBody' => 'Deleted']],
        ];
    }

    protected function invalidRouter()
    {
        return [
            'Array' => [['NonExistingController', 'index']],
            'String' => ['NonExistingController'],
         ];
    }

    protected function validRouter()
    {
        return [
            'Array' => [[TestControllerExistAction::class, 'existingMethod']],
            'String' => [TestControllerActionInvoke::class],
         ];
    }

}
 class TestControllerNoAction
 {
     // Intencionalmente deixe este controlador vazio ou adicione métodos diferentes do que será testado
 }

  class TestControllerExistAction
  {
      public function existingMethod(ServerRequestInterface $request): ResponseInterface
      {
          // Retorna uma resposta simulada para o teste
          return new TestResponse(200, [], 'Success');
      }
  }

   class TestControllerActionInvoke
   {
       public function __invoke(ServerRequestInterface $request): ResponseInterface
       {
           // Retorna uma resposta simulada para o teste
           return new TestResponse(200, [], 'Success');
       }
   }

use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class TestMiddleware implements MiddlewareInterface
{
    private bool $throwException;

    public function __construct(bool $throwException = false)
    {
        $this->throwException = $throwException;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($this->throwException) {
            throw new \RuntimeException('Middleware exception');
        }

        // You can modify the request or response here if needed

        return $handler->handle($request);
    }
}
   
  class TestResponse implements ResponseInterface
  {
      private int $statusCode;
      private string $reasonPhrase;
      private array $headers;
      private StreamInterface $body;
  
      public function __construct(int $statusCode = 200, array $headers = [], string $body = 'Success')
      {
          $this->statusCode = $statusCode;
          $this->headers = $headers;
          $this->body = $this->createStream($body);
          $this->reasonPhrase = '';
      }
  
      private function createStream(string $body): StreamInterface
      {
          return new class($body) implements StreamInterface {
              private string $body;
  
              public function __construct(string $body)
              {
                  $this->body = $body;
              }
  
              public function __toString()
              {
                  return $this->body;
              }
  
              public function close() {}
              public function detach() {}
              public function getSize() {}
              public function tell() {}
              public function eof() {}
              public function isSeekable() { return false; }
              public function seek($offset, $whence = SEEK_SET) {}
              public function rewind() {}
              public function isWritable() { return false; }
              public function write($string) {}
              public function isReadable() { return true; }
              public function read($length) {}
              public function getContents() { return $this->body; }
              public function getMetadata($key = null) {}
          };
      }
  
      public function getStatusCode()
      {
          return $this->statusCode;
      }
  
      public function withStatus($code, $reasonPhrase = '')
      {
          $new = clone $this;
          $new->statusCode = $code;
          $new->reasonPhrase = $reasonPhrase;
          return $new;
      }
  
      public function getReasonPhrase()
      {
          return $this->reasonPhrase;
      }
  
      public function getProtocolVersion()
      {
          return '1.1';
      }
  
      public function withProtocolVersion($version)
      {
          // Implement as needed
      }
  
      public function getHeaders()
      {
          return $this->headers;
      }
  
      public function hasHeader($name)
      {
          return isset($this->headers[$name]);
      }
  
      public function getHeader($name)
      {
          return $this->headers[$name] ?? [];
      }
  
      public function getHeaderLine($name)
      {
          return implode(', ', $this->getHeader($name));
      }
  
      public function withHeader($name, $value)
      {
          $new = clone $this;
          $new->headers[$name] = (array) $value;
          return $new;
      }
  
      public function withAddedHeader($name, $value)
      {
          $new = clone $this;
          $new->headers[$name][] = $value;
          return $new;
      }
  
      public function withoutHeader($name)
      {
          $new = clone $this;
          unset($new->headers[$name]);
          return $new;
      }
  
      public function getBody()
      {
          return $this->body;
      }
  
      public function withBody(StreamInterface $body)
      {
          $new = clone $this;
          $new->body = $body;
          return $new;
      }
  }