<?php

use Minimalist\Router\ControllerFactory;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class ControllerFactoryTest extends TestCase
{

    private ControllerFactory $controllerFactory;
    protected function setUp(): void
    {
        parent::setUp();
        $containerInterfaceMock = $this->createMock(ContainerInterface::class);
        $this->controllerFactory = new ControllerFactory($containerInterfaceMock);
    }

    public function testCreateThrowsExceptionForNonInstantiableClass()
    {
        $abstractClass = NonInstantiableClass::class;

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage("Class $abstractClass is not instantiable");

        $this->controllerFactory->create($abstractClass);
    }

    public function testCreateWithoutConstructor()
    {
        $classWithoutConstructor = SimpleClass::class;

        $instance = $this->controllerFactory->create($classWithoutConstructor);

        // Verifica se a instância é do tipo da classe esperada
        $this->assertInstanceOf($classWithoutConstructor, $instance);
    }

    public function testCreateThrowsExceptionForBuiltInType()
    {
        $classWithBuiltInType = ClassWithBuiltInType::class;

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage("Cannot resolve built-in type: someInt");

        $this->controllerFactory->create($classWithBuiltInType);
    }

    public function testCreateThrowsExceptionWhenClassNotInContainer()
    {
        $classWithDependency = ClassWithDependency::class;

        // Mock the ContainerInterface to simulate that it does not have the dependency registered
        $containerInterfaceMock = $this->createMock(ContainerInterface::class);
        $containerInterfaceMock->method('has')->willReturn(false);

        $this->controllerFactory = new ControllerFactory($containerInterfaceMock);

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage("Cannot resolve class: SomeDependency");

        $this->controllerFactory->create($classWithDependency);
    }

    public function testCreateThrowsExceptionWhenCannotResolveDependency()
    {
        $classWithDependency = ClassWithDependency::class;
        $className = SomeDependency::class;
    
        // Mock the ContainerInterface
        $containerInterfaceMock = $this->createMock(ContainerInterface::class);
    
        // Simulate that the container does have the dependency
        $containerInterfaceMock->method('has')
            ->will($this->returnCallback(function ($class) use ($className) {
                return $class === $className;
            }));
    
        // Simulate that the container does not return an instance for the dependency
        $containerInterfaceMock->method('get')
            ->will($this->returnCallback(function ($class) use ($className) {
                if ($class === $className) {
                    throw new \RuntimeException("Cannot resolve class: $class");
                }
                return new \stdClass(); // Dummy value for other cases
            }));
    
        $this->controllerFactory = new ControllerFactory($containerInterfaceMock);
    
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage("Cannot resolve class: $className");
    
        $this->controllerFactory->create($classWithDependency);
    }

    public function testCreateWithValidDependencies()
    {
        $classWithDependencies = ClassWithDependencies::class;
        $dependencyClass = DependencyClass::class;

        // Mock the ContainerInterface
        $containerInterfaceMock = $this->createMock(ContainerInterface::class);

        // Mock that the container provides the dependency class
        $containerInterfaceMock->method('has')
            ->will($this->returnCallback(function ($class) use ($dependencyClass) {
                return $class === $dependencyClass;
            }));

        // Mock the container to return an instance of DependencyClass
        $containerInterfaceMock->method('get')
            ->will($this->returnCallback(function ($class) use ($dependencyClass) {
                if ($class === $dependencyClass) {
                    return new DependencyClass();
                }
                throw new \RuntimeException("Cannot resolve class: $class");
            }));

        $this->controllerFactory = new ControllerFactory($containerInterfaceMock);

        // Create an instance of the class with dependencies
        $instance = $this->controllerFactory->create($classWithDependencies);

        // Verify that the instance is of the expected class
        $this->assertInstanceOf($classWithDependencies, $instance);
    }
    public function testCreateThrowsRuntimeExceptionOnReflectionException()
    {
        // Classe fictícia para simular um erro de reflexão
        $nonExistentClass = 'SomeNonExistentClass';
    
        // Mock the ContainerInterface
        $containerInterfaceMock = $this->createMock(ContainerInterface::class);
    
        // Instantiate the ControllerFactory with the mocked container
        $this->controllerFactory = new ControllerFactory($containerInterfaceMock);
    
        // Expect a RuntimeException to be thrown due to ReflectionException
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage("Error resolving dependencies for $nonExistentClass: Class \"$nonExistentClass\" does not exist");    
        // Trigger the exception
        $this->controllerFactory->create($nonExistentClass);
    }    

}

abstract class NonInstantiableClass
{
}
class SimpleClass
{
}

class ClassWithBuiltInType
{
    public function __construct(int $someInt)
    {
    }
}

class ClassWithDependency
{
    public function __construct(SomeDependency $dependency)
    {
        // Constructor with a class dependency
    }
}

class SomeDependency
{
    // A simple class to act as a dependency
}

class ClassWithDependencies
{
    private DependencyClass $dependency;

    public function __construct(DependencyClass $dependency)
    {
        $this->dependency = $dependency;
    }
}

class DependencyClass
{
    // A simple class to act as a dependency
}